import * as React from 'react';
import { Tablelist } from './components/tableList';
import './App.css';

function App() {
  return (
    <div className="App">
      <Tablelist />
    </div>
  );
}

export default App;
