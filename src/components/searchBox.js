import React from 'react';
import { Container, Typography, Grid, Box, Button, TextField } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { searchData } from './../JSON/data.json';
import SearchIcon from '@material-ui/icons/Search';
import RotateLeftSharpIcon from '@material-ui/icons/RotateLeftSharp';



const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    btnRoot: {
        '& > *': {
            margin: theme.spacing(1),
            padding: '10px 20px',
            width: '15%'
        },
    }
}));
export default function SearchBox() {
    const classes = useStyles();
    return (
        <Container>
            <Box m={4} >

                <Grid container spacing={3} className={classes.root}>
                    {searchData?.map(ele =>
                        <Grid item key={ele.labelName}xs={6} md={4} lg={4}>
                            <Grid
                                container
                                direction="row"
                                xs={12}
                                item
                                alignItems="center"
                            >
                                <Grid item xs={3} md={3} lg={4}>
                                    <Typography  component="span"> {ele.labelName} </Typography>
                                </Grid>
                                <Grid item xs={9} md={9} lg={8}>
                                    <TextField size="small" color="secondary" id="outlined-basic" label={ele.inputField}  variant="outlined" />
                                </Grid>

                            </Grid>
                        </Grid>
                    )}
                </Grid>

                <Box className={classes.btnRoot} component="div" mt={4} justifyContent="flex-end" display="flex">
                    <Button variant="contained" size="medium" color="secondary" startIcon={<SearchIcon />}>
                        Search
                    </Button>
                    <Button variant="contained" size="medium" color="secondary" startIcon={<RotateLeftSharpIcon />}>
                        Reset
                    </Button>
                </Box>

            </Box>
        </Container>
    )
}
