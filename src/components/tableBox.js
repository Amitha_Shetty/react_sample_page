import React from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import { Table, TableBody, TableCell, TableContainer, Box, TableHead, TableRow, Paper, Radio, Button } from '@material-ui/core'
import { tableData, tableHeaderCell } from './../JSON/data.json';


const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.action.disabled,
    color: theme.palette.text.primary,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);


const useStyles = makeStyles((theme) => ({
  table: {
    minWidth: 700,
  },
  tableBtnroot: {
    '& > *': {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.background.paper,
      color: theme.palette.info.main
    },
  },
 
}));

export default function TableBox() {
  const classes = useStyles();
  const [selectedValue, setSelectedValue] = React.useState(0);

  const handleChange = (index) => {
    setSelectedValue(index);
  };
  return (
    <TableContainer component={Paper}>
      <Box display="flex" justifyContent="flex-end" className={classes.tableBtnroot} bgcolor="secondary.main">
      <Button variant="contained">
        XL
      </Button>
      <Button variant="contained">
        Divisions
      </Button>
      <Button variant="contained">
        Details
      </Button>
      <Button variant="contained">
        Shipments
      </Button>
      </Box>
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
            {tableHeaderCell.map(ele =>
              <StyledTableCell key={ele.name} align={ele.align}>{ele.name}</StyledTableCell>
            )}
          </TableRow>
        </TableHead>
        <TableBody>
          {tableData.map((row, index) => (
            <StyledTableRow key={index}>
              <StyledTableCell>
                <Radio aria-label="select" checked={selectedValue === index} name="selectTable" onChange={() => handleChange(index)} />
              </StyledTableCell>
              <StyledTableCell align="left">{row.poNo} {index}</StyledTableCell>
              <StyledTableCell align="left">{row.prNo}</StyledTableCell>
              <StyledTableCell align="center">{row.createDate}</StyledTableCell>
              <StyledTableCell align="center">{row.location}</StyledTableCell>
              <StyledTableCell align="center">{row.totalItems}</StyledTableCell>
              <StyledTableCell align="center">{row.totalQty}</StyledTableCell>
              <StyledTableCell align="center">{row.totalCost}</StyledTableCell>
              <StyledTableCell align="center">{row.shortQty}</StyledTableCell>
              <StyledTableCell align="center">{row.exccessQty}</StyledTableCell>
              <StyledTableCell align="center">
                <Button variant="outlined" color="secondary">
                  Rejected
      </Button>
              </StyledTableCell>
            </StyledTableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
