import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid,  Container, Typography, Box } from '@material-ui/core';
import SearchBox from './searchBox';
import TableBox from './tableBox';


const defaultProps = {
    bgcolor: 'background.paper',
    border: 2
  };
const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    }
}));



export function Tablelist() {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <Container fixed>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <Box mt={2}>
                            <Typography variant="h6" component="h6">
                                SEARCH ORDER
                                <Box borderColor="secondary.main" {...defaultProps}>
                                    <SearchBox/>
                                </Box>
                           </Typography>
                        </Box>
                    </Grid>
                    <Grid item xs={12}>
                    <Box mt={2}>
                            <Typography variant="h6" component="h6">
                                SEARCH RESULT
                                <Box borderColor="secondary.main">
                                    <TableBox/>
                                </Box>
                           </Typography>
                        </Box>
                    </Grid>
                </Grid>
            </Container>
        </div>
    );
}